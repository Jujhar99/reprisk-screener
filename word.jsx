﻿/*
 RepRisk Screener
 Made by Singh
 jujhar@rebabre.com
 Dec 5 2017
 */
var React = require('react');
var ReactDOM = require('react-dom');
import request from 'graphql-request'

const query = `
query{
  articles {
    title,
    html,
    date,
    source {
      id,
      name,
      description,
      location
    },
    companies {
      id,
      name,
      website,
      location
    }
  }
}
`

// Complete request then load component
var articleTitle, articleDate, articleHTML;
var articleSourceInfo, articleCompaniesInfo;

request('https://js-assignment.reprisk.com/graphql', query).then(data => {
    var dat = data;
    console.log(dat.articles);
    console.log(dat.articles[0].source);
    articleTitle = dat.articles[0].title;
    console.log("d-" + dat.articles[0].date);
    var myDate = new Date(dat.articles[0].date * 1000);
    articleDate = myDate.toDateString();
    articleSourceInfo = dat.articles[0].source;
    articleCompaniesInfo = dat.articles[0].companies;
    articleHTML = dat.articles[0].html;
    // Clean up and sanitize document -> sanitation could be improved
    articleHTML = articleHTML.replace("<nav", "<nav style='display:none' ");
    articleHTML = articleHTML.replace('id="user-menu"', "id='user-menu' style='display:none' ");
    articleHTML = articleHTML.replace('class="main-nav-item screenreader-link"', 'class="main-nav-item screenreader-link" style="display:none" ');
    articleHTML = articleHTML.replace('class="main-nav-item screenreader-link"', 'class="main-nav-item screenreader-link" style="display:none" ');
    articleHTML = articleHTML.replace('class="side-nav"', 'class="side-nav" style="display:none" ');
    articleHTML = articleHTML.replace(/class="chain-content no-skin clear"/gi, 'class="chain-content no-skin clear" style="display:none" ');
    articleHTML = articleHTML.replace(/id="f0JRbCYY9Fuvzq"/gi, 'id="f0JRbCYY9Fuvzq" style="display:none" ');
    articleHTML = articleHTML.replace('class="subTitle-follow-modal"', 'class="subTitle-follow-modal" style="display:none" ');
    articleHTML = articleHTML.replace('"', 'class="subTitle-follow-modal" style="display:none" ');
    articleHTML = articleHTML.replace('class="subTitle-follow-modal"', 'class="subTitle-follow-modal" style="display:none" ');
    articleHTML = articleHTML.replace('collection.find(', '');
    articleHTML = articleHTML.replace('INSERT INTO', '');
    articleHTML = articleHTML.replace('SET (', '');
    articleHTML = articleHTML.replace('SELECT *', '');
    articleHTML = articleHTML.replace('<script>', '<div style="display: none">');
    articleHTML = articleHTML.replace('</script>', '</div style="display: none">');
    //articleHTML = "<h3>SUp</h3>"
    render();
    return;
})


var boxStyle = {
    flex: 1,
    overflowY: "scroll",
    height: "570px",
    paddingLeft: "20px",
    paddingRight: "20px"
};

var boxStyleAlt = {
    flex: 1,
    overflow: "hidden",
    height: "600px",
    paddingLeft: "20px"
};

var companyInfoStyle = {
    width: "230px"
}

class Index extends React.Component {
    render() {
        return (
            <div>
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/logo.svg" height="100px;" alt="RepRisk logo"/>
                <br />

                <br /><br />

                <div style={{display:"flex"}}>
                    <div style={boxStyle}>
                        <div dangerouslySetInnerHTML={{ __html: (this.props.articleHTML) }}></div>
                    </div>

                    <div style={boxStyleAlt}>
                        <h4>Source Information</h4>
                        Name<br />
                        {this.props.articleSourceInfo.name}
                        <br /><br />

                        Location<br />
                        {this.props.articleSourceInfo.location}
                    <span style={{paddingLeft:"100px"}}>
                        <img src={"assets/flags/"+
                        this.props.articleSourceInfo.location+".svg"}
                             alt="country flag"
                             style={{height:"20px"}}
                        />
                    </span>
                        <br /><br />

                        Description<br />
                        {this.props.articleSourceInfo.description}
                        <br /><br />

                        Article Date<br />
                        {this.props.articleDate}
                        <br /><br />

                    </div>

                    <div style={boxStyleAlt}>
                        <h4>Companies</h4>

                        Name<br />
                        <span style={companyInfoStyle}>
                        {this.props.articleCompaniesInfo[0].name}
                            &nbsp;&nbsp;
                            <a href={this.props.articleCompaniesInfo[0].website}
                               target="_blank">
                                <img src="assets/external-link.svg"
                                     style={{height:"12px"}}
                                     alt="external link"/>
                            </a>
                        </span>

                        <img src={"assets/flags/"+this.props.articleCompaniesInfo[0].location+".svg"}
                             alt={this.props.articleCompaniesInfo[0].location}
                             style={{height:"20px",marginLeft:"60px"}}/>
                        <br /><br /><br />

                        Name<br />
                        <span style={companyInfoStyle}>
                        {this.props.articleCompaniesInfo[1].name}
                            &nbsp;&nbsp;
                            <a href={this.props.articleCompaniesInfo[1].website}
                               target="_blank">
                                <img src="assets/external-link.svg"
                                     style={{height:"12px"}}
                                     alt="external link"/>
                            </a>
                        </span>

                        <img src={"assets/flags/"+this.props.articleCompaniesInfo[1].location+".svg"}
                             alt="company flag"
                             style={{height:"20px",marginLeft:"60px"}}/>
                        <br /><br /><br />

                        Name<br />
                        <span style={companyInfoStyle}>
                        {this.props.articleCompaniesInfo[2].name}
                            &nbsp;&nbsp;
                            <a href={this.props.articleCompaniesInfo[2].website}
                               target="_blank">
                                <img src="assets/external-link.svg"
                                     style={{height:"12px"}}
                                     alt="external link"/>
                            </a>
                        </span>

                        <img src={"assets/flags/"+this.props.articleCompaniesInfo[2].location+".svg"}
                             alt="company flag"
                             style={{height:"20px",marginLeft:"60px"}}/>
                        <br /><br /><br />

                        Name<br />
                        <span style={companyInfoStyle}>
                        {this.props.articleCompaniesInfo[3].name}
                            &nbsp;&nbsp;
                            <a href={this.props.articleCompaniesInfo[3].website}
                               target="_blank">
                                <img src="assets/external-link.svg"
                                     style={{height:"12px"}}
                                     alt="external link"/>
                            </a>
                        </span>

                        <img src={"assets/flags/"+this.props.articleCompaniesInfo[3].location+".svg"}
                             alt="company flag"
                             style={{height:"20px",marginLeft:"60px"}}/>
                        <br /><br /><br />


                        <br /><br /><br /><br /><br />
                    </div>
                </div>

            </div>
        );
    }
}

function render() {
    ReactDOM.render(<Index articleTitle={articleTitle}
                           articleDate={articleDate}
                           articleHTML={articleHTML}
                           articleSourceInfo={articleSourceInfo}
                           articleCompaniesInfo={articleCompaniesInfo}
    />, document.getElementById("react"))
}